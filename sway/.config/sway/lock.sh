#!/bin/bash
# swaylock with blurred background

if [ -x "$(command -v convert)" -a -x "$(command -v grim)" ];then
  PIC_LOCATION="$(mktemp --suffix=.jpg -p $XDG_RUNTIME_DIR)"
  grim -s 0.1 - | convert -scale 500% -blur 0x15 - $PIC_LOCATION
  swaylock -f -i $PIC_LOCATION --indicator-radius=100 --indicator-idle-visible
  shred -zu $PIC_LOCATION
else
  swaylock -f -c 000000 --indicator-radius=100 --indicator-idle-visible
fi
