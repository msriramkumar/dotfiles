#!/bin/bash
# Script to run the system updates

echo -e "\n=======Checking flatpak for updates=======\n"
flatpak update;

echo -e "\n=======Checking pacman for updates========\n"
sudo pacman -Syu;

# Send custom signal to waybar module to reexecute package update count script
pkill -RTMIN+2 waybar;
