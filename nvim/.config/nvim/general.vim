syntax on
syntax enable

filetype plugin on

inoremap jk <ESC>

set hlsearch
set relativenumber
set ignorecase
set backspace=indent,eol,start " allow backspacing over everything in insert mode
set nobackup		" do not keep a backup file, use versions instead
set nowritebackup
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set cmdheight=1
set nrformats-=octal
set ttimeout
set ttimeoutlen=100
set ffs=unix,dos,mac " Unix as standard file type
set termencoding=utf-8
set encoding=utf-8
set fileencoding=utf-8
set so=5 " scroll lines above/below cursor
set sidescrolloff=5
set lazyredraw
set magic " for regular expressions
set autoread
set sessionoptions-=options
set hid " buffer becomes hidden when abandoned
set clipboard=unnamedplus
set completeopt=longest,menuone,preview
set smarttab
set cindent
set tabstop=2
set shiftwidth=2
set expandtab " always uses spaces instead of tab characters
set statusline=%<%f%h%m%r\ [%{expand(&filetype)}]\ [Buf:%n]%=\ L\:%l/%L\ C\:%c%V\ \ %P

autocmd BufNewFile,BufReadPost *.md set filetype=markdown  " detect .md as markdown instead of modula-2
autocmd BufNewFile,BufRead,BufEnter *.md,*.markdown :syntax match markdownIgnore "_" " stop highlighting of underscores in markdown files

if has('mouse')
  set mouse=a
endif

if has("autocmd")
  augroup vimrcEx
    au!
    autocmd FileType text setlocal textwidth=108
    autocmd BufWritePre * %s/\s\+$//e " Trim whitespace onsave

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    " Also don't do it when the mark is in the first line, that is the default
    " position when opening a file.
    autocmd BufReadPost *
          \ if line("'\"") > 1 && line("'\"") <= line("$") |
          \   exe "normal! g`\"" |
          \ endif

  augroup END
endif " has("autocmd")

if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif

if has('path_extra')
  setglobal tags-=./tags tags^=./tags;
endif

if &history < 1000
  set history=1000
endif

if &tabpagemax < 50
  set tabpagemax=50
endif

if !empty(&viminfo)
  set viminfo^=!
endif
