colorscheme onedark

"====== NERDTree ========
"========================
" Exit Vim if NERDTree is the only window left.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
    \ quit | endif

noremap <C-n> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
"========= CoC ==========
"========================
let g:coc_global_extensions = ['coc-json', 'coc-tsserver', 'coc-css', 'coc-prettier', 'coc-snippets', 'coc-eslint', 'coc-pairs']

" Use tab for trigger completion with characters ahead and navigate.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

command! -nargs=0 Prettier :CocCommand prettier.formatFile

"====== NERD Commenter =======
"=============================
nmap ;; <Plug>NERDCommenterToggle<CR>
vmap ;; <Plug>NERDCommenterToggle<CR>

" For react projects that use .js instead of .jsx -_-
let g:NERDCustomDelimiters = { 'javascript': { 'left': '//' ,'leftAlt': "{/*", 'rightAlt': "*/}" } , 'c': { 'left': '/**','right': '*/' } }

" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

"====== dmenu ===============
"============================
" Strip the newline from the end of a string
function! Chomp(str)
  return substitute(a:str, '\n$', '', '')
endfunction

" Find a file and pass it to cmd
function! DmenuOpen(cmd)
  let fname = Chomp(system("find -type f | dmenu -i -l 15 -fn 'Fira Code:size=12' -nb '#000' -nf '#fff' -sb '#fff' -sf '#000' -p " . a:cmd))
  if empty(fname)
    return
  endif
  execute a:cmd . " " . fname
endfunction
map <c-p> :call DmenuOpen("e")<cr>
