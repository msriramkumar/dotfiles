# dotfiles
My personal config files...

## Steps:
clone and exec post_install.sh

## AMDGPU Power Profile
Tweaking the power profile is absolutely essential for me to get VRR working properly.
Without this tweak, monitor OSD for the refresh rate reading will fluctuate wildly when in game FPS is locked to a value much lower than the screen refresh rate.
This results in intense microstutters resulting in a insanely choppy experience.

Below udev rule sets the card to "manual" mode with power profile set to "1". More info on : https://wiki.archlinux.org/title/AMDGPU#Performance_levels
`
# /etc/udev/rules.d/30-amdgpu.rules
KERNEL=="card1", SUBSYSTEM=="drm", DRIVERS=="amdgpu", ATTR{device/power_dpm_force_performance_level}="manual", ATTR{device/pp_power_profile_mode}="1"
`

Note:
You can run "cat /sys/class/drm/card1/device/power_dpm_force_performance_level" to check the current level.
"card1" may change depending on where the graphics card is installed. It might as well be "card0","card2", etc in your case.

## Ownership:
I do not in any way own the images present in **"wallpaper"**. All credits goes to the original artists.
If I am violating any copyright, please let me know and I will remove them from the repository.
