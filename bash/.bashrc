#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ -f ~/.git-prompt.sh ]] && . ~/.git-prompt.sh
[[ -f ~/.winerc ]] && . ~/.winerc

HISTSIZE=5000
HISTFILESIZE=10000

# Env Vars
export EDITOR=nvim
export VISUAL=nvim
export TERM=alacritty
export BROWSER=librewolf
export MOZ_ENABLE_WAYLAND=1
export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:~/.local/bin"

# BASH Options
shopt -s promptvars
shopt -s cdspell  	# correct cd typos
shopt -s extglob 	# Extended pattern
shopt -s autocd		# auto cd
shopt -s extglob
shopt -s histappend

# TAB Select
bind TAB:menu-complete

# Functions
takefunc()
{
    mkdir -p "$1";
    cd "$1";
}

#aliases
#pacman
alias p="sudo pacman"
alias pi="sudo pacman -S"
alias pr="sudo pacman -Rsn"
alias pc="sudo pacman -Sc"
alias pu="sudo pacman -Syu"
alias pq="pacman -Ss"

#misc
alias ls='ls --color=auto'
alias rs="krs;redshift &"
alias krs="pkill redshift"
alias take="takefunc"
alias cemacs="emacs -nw"
alias vim="nvim"
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"
alias rmrf="rm -rf"
alias best="mpv --profile=perf"
alias video="yt-dlp -o '%(title)s.%(ext)s' -f 'bestvideo[fps=60][height<=1440]+bestaudio/bestvideo+bestaudio'"
alias music="yt-dlp -o '%(title)s.%(ext)s' -x --audio-format mp3 --audio-quality 0"

PS1='\W\[\033[32m\]$(__git_ps1)\[\033[00m\] >> '
