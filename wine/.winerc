#!/bin/bash
# My WINE settings (mainly for gaming)

# Vars & Flags
WINE_BOTTLES_HOME="/media/Stuff/wine"
BACKUP_HOME="/media/Stuff/backup-saves"
# WINE_DIST="/usr/bin"                    # System wine
WINE_DIST="$HOME/Downloads/wine-ge-custom/bin"
DXVK_HOME="$HOME/Downloads/dxvk"        # https://github.com/doitsujin/dxvk
VKD3D_HOME="$HOME/Downloads/vkd3d"      # https://github.com/HansKristian-Work/vkd3d-proton

export DXVK_OSD="fps,gpuload,memory,scale=0.65"
export DXVK_FPS=0
export WINE_ESYNC=1
export SILENT=1
export DXVK=1
export RES="1080"

# Aliases
alias lspfx="ls '$WINE_BOTTLES_HOME'"
alias rmpfx="deletewineprefix"
alias wbkp="takebackup"
alias wconfig="configurewineprefix"
alias wpfx="createwineprefix"
alias wrun="openwithwine"
alias ES="WINEESYNC"

# Functions
createwineprefix()
{
    WP_DIR="$WINE_BOTTLES_HOME/$1"

    checkprefix "$1" "$WP_DIR"
    [ $? -eq 1 ] || return 1

    mkdir -p "$WP_DIR"

    WINEPREFIX="$WP_DIR" "$WINE_DIST/wineboot" -i && WINEPREFIX="$WP_DIR" "$WINE_DIST/wineserver" --wait;
    setupdxvk "$WP_DIR";

    WINE_VER=$("$WINE_DIST/wine" --version)
    echo "Created with $WINE_VER" >> "$WP_DIR/info.txt"
}

setupdxvk()
{
  if [ "$DXVK" -eq "1" ]; then
      \cp "$DXVK_HOME/x64/"* "$1/drive_c/windows/system32" &&
      \cp "$DXVK_HOME/x32/"* "$1/drive_c/windows/syswow64" &&
      sleep 5 &&
      PATH="$PATH:$WINE_DIST" WINEPREFIX="$1" "$VKD3D_HOME/setup_vkd3d_proton.sh" install
  fi
}

deletewineprefix(){
  WP_DIR="$WINE_BOTTLES_HOME/$1";

  checkprefix "$1" "$WP_DIR"
  [ $? -eq 0 ] || return 1

  read -p "Are you sure you want to delete '$1' [N/Yes] " p;

  if [ "${p,,}" = "yes" ]; then
      takebackup "$1";
      rm -rf "$WP_DIR";

      echo "Prefix deleted";
      return 0;
  fi

  echo "Prefix not deleted";
}

takebackup(){
  WP_DIR="$WINE_BOTTLES_HOME/$1";

  checkprefix "$1" "$WP_DIR";
  [ $? -eq 0 ] || return 1

  BKP_DIR="$BACKUP_HOME/$1__$(date +'%Y-%m-%d_%H:%M:%S')";
  mkdir -p "$BKP_DIR"

  cp -r "$WP_DIR"/drive_c/{ProgramData,users/!(Public)/{AppData,Documents,Saved\ Games}} "$BKP_DIR";
}

setresolution(){
    case $RES in
      "2k" | "QHD" | "1440p")
        xrandr -s 2560x1440
      ;;
      "4k" | "UHD")
        xrandr -s 3840x2160
      ;;
    esac

    xrandr --output "$(xrandr --listactivemonitors | awk 'END{print $NF}')" --set TearFree off
    RESOLUTION=$(xrandr | grep "*" | awk '{print $1}');
}

postexit(){
    WINEPREFIX="$WP_DIR" "$WINE_DIST/wineserver" -k;
    xrandr --output "$(xrandr --listactivemonitors | awk 'END{print $NF}')" --set TearFree on
    xrandr -s 0;
}

openwithwine()
{
    WP_DIR="$WINE_BOTTLES_HOME/$1"

    checkprefix "$1" "$WP_DIR"
    [ $? -eq 0 ] || return 1

    ARGS=("$@")
    EPIC_ARGS=(
      "-EpicPortal"
    )

    REDIRECT="/dev/null";
    [ "$SILENT" = "0" ] && REDIRECT="/dev/stdout";

    setresolution;

    WINEDLLOVERRIDES="xaudio2_7,d3d11,d3d12,d3d12core,d3d10core,dxgi,d3d9,winegstreamer=n,b;mscoree,mshtml,winebrowser.exe,winemenubuilder.exe=" DXVK_HUD="$DXVK_OSD" DXVK_FRAME_RATE="$DXVK_FPS" WINEESYNC="$WINE_ESYNC" VKD3D_DISABLE_EXTENSIONS="VK_KHR_present_id,VK_KHR_present_wait" VK_INSTANCE_LAYERS="VK_LAYER_MESA_overlay" VK_LAYER_MESA_OVERLAY_CONFIG="fps,position=top-right" WINEPREFIX="$WP_DIR" firejail --deterministic-shutdown --net=none "$WINE_DIST/wine" "explorer" "/desktop=game,$RESOLUTION" "$2" "${EPIC_ARGS[@]}" "${ARGS[@]:2}" 1>"$REDIRECT" 2>&1;

    postexit;
}

configurewineprefix(){
  echo -e "Available prefixes:\n$(lspfx)"
    read -p "Enter your prefix: " pfx

    WP_DIR="$WINE_BOTTLES_HOME/$pfx";

    checkprefix "$pfx" "$WP_DIR"
    [ $? -eq 0 ] || return 1

    WINEPREFIX="$WP_DIR" "$WINE_DIST/winecfg"
}

checkprefix(){
  if [[ -z "$1" || ! -d "$2" ]]; then
    echo "Prefix not found";
    return 1
  fi
}
