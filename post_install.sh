#!/bin/bash -e
# My install script for arch

[[ -z "$HOME" ]] && echo -e "\$HOME is not defined.\nIt should be defined to provide a customized experience ONLY the user running this script.\nExiting now." && exit 1

pkgs=(
"alacritty"
"bash-completion"
"dunst"
"flatpak"
"git"
"grim"
"gst-plugin-pipewire"
"imagemagick"
"jq"
"libappindicator-gtk3"
"libva-mesa-driver"
"linux-firmware"
"man-db"
"mate-polkit"
"mpv"
"nano"
"networkmanager"
"newsboat"
"noto-fonts"
"pacman-contrib"
"papirus-icon-theme"
"pavucontrol"
"pcmanfm-gtk3"
"pipewire"
"pipewire-alsa"
"pipewire-pulse"
"pop-gtk-theme"
"pop-icon-theme"
"qt5-wayland"
"slurp"
"stow"
"sway"
"swaybg"
"swayidle"
"swaylock"
"ttf-nerd-fonts-symbols-mono"
"udiskie"
"udisks2"
"waybar"
"wireplumber"
"wl-clipboard"
"wlsunset"
"wofi"
"yt-dlp"
)


echo "Preparing to install packages..."

sudo pacman -Syy "${pkgs[@]}" --noconfirm --needed

echo -e "Finished installing packages.\nSymlinking dotfiles."

read -p "Going to symlink dotfiles to complete the setup. THIS CAN AND WILL OVERWRITE ANY CHANGES WHICH YOU HAVE ALREADY MADE TO THE AVAILABLE DOTFILES...CONTINUE? [y/n]" confirmation

if [ "$confirmation" = "y" ];then
  stow -t $HOME */ --adopt

  # restore git to overwrite symlinks with repo code.
  #
  git restore .

  echo -e "\nFinished.Have fun!\n"
  exit 0
fi

echo "Dotfiles not symlinked... Handle it yourself !"
